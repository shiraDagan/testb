@extends('layouts.app')
@section('content')

<h1>This is a products list</h1>
 
@cannot('user')<h4><a href = "{{route('products.create')}}">   create a new product </a></h4>@endcannot


<table class = 'table'>
 <thead> 
  
      <tr >  
      <th>name</th> 
      <th>price</th> 
      <th >Delete</th>
      <th>edit</th>
     
      
       </tr>  
  </thead>

  @foreach($products as $product)
  <tr>
  
  <td>  
  
              @if ($product->status)
                <input type = 'checkbox'  id ="{{$product->id}}" checked>
              @else
                <input type = 'checkbox' id ="{{$product->id}}">
               @endif
               {{$product->name}} 
              </td>        

  <!-- <td  >{{$product->name}}</td> -->
  <td  >{{$product->price}}</td>
  
  
  @cannot('user')<td  ><a href="{{route('delete', $product->id)}}">Delete</a></td>@endcannot
  

 

   @cannot('user')<td  ><a href ="{{route('products.edit', $product ->id)}}" >edit</a></td>@endcannot
    
  
  </tr>
  @endforeach
  <script>
       $(document).ready(function(){
           $(":checkbox").click(function(event){
               $.ajax({
                   url: "{{url('products')}}" + '/' + event.target.id ,
                   dataType:'json' ,
                   type:'put',
                   contentType:'application/json',
                   data: JSON.stringify({'status':event.target.checked, _token:'{{csrf_token()}}'}),
                   processData:false,
                   success: function( data){
                        console.log(JSON.stringify( data ));
                   },
                   error: function(errorThrown ){
                       console.log( errorThrown );
                   }
               });               
            });
       });
</script>  



   
</table>

@endsection
