@extends('layouts.app')
@section('content')

<h1>Edit a product</h1>
<form method = 'post' action = "{{action('ProductController@update', $product->id)}}" >
@csrf    
@method('PATCH')    
<div class = "form-group">    
    <label for = "name">name product to update </label>
    <input type = "text" class = "form-control" name = "name" value = "{{$product->name}}">

    <label for = "price">price product to update </label>
    <input type = "text" class = "form-control" name = "price" value = "{{$product->price}}">

</div>

<div class = "form-group">    
    <input type = "submit" class = "form-control" name = "submit" value = "Update">
</div>

</form>

@endsection
