<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use App\User;
use App\Product;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;


class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
        $id = Auth::id();
        $products = Product::all();
        return view('products.index', ['products'=>$products,'id' =>$id]);


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::denies('owner')) {
            abort(403,"Sorry you are not allowed to create products..");
        }
 
        return view ('products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      

        //$id = 1;
        $product = new Product();
        $id =Auth::id();
        $product->name = $request->name;
        $product->price = $request->price;
        $product->status=0;
        $product->user_id = $id;
        $product->save();
        return redirect('products');  

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::denies('owner')) {
            abort(403,"You are not allowed to edit products..");
        }
        $product = Product::findOrFail($id);
        $id = Auth::id();
        return view('products.edit', compact('product'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    { 
        $products=Product::findOrFail($id);
        $products->update($request->except(['_token']));
        if ($request->ajax()){
            return Response::json(array('result'=>'success', 'status' => $request->status),200);
        }     
        
        return redirect('products');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::denies('owner')) {
            abort(403,"You are not allowed to delete products");
        }
        $product = Product::findOrFail($id);
        // if(!$books->user->id == Auth::id()) return(redirect('books'));
         $product->delete(); 
         return redirect('products');   

    }
   
    public function statusUpdate($id){
     


        $products=Product::findOrFail($id);
        $products->update($request->except(['_token']));
        if ($request->ajax()){
            return Response::json(array('result'=>'success', 'status' => $request->status),200);
        }
        $products->status==1;
        $products->update();
        return redirect('products');
    }
    
}
